# Create by gravifoxita #

import socket
import psutil

dangerous_ports = [21, 22, 23, 25, 53, 80, 135, 443, 445, 1433, 1521, 3306, 3389, 5900, 8080]

ip = socket.gethostbyname(socket.gethostname())
print("IP: ", ip)

open_ports = []
for conn in psutil.net_connections():
   if conn.status == 'LISTEN':
      open_ports.append(conn.laddr.port)

if open_ports:
   print("\Ports open :\n")
   for ports in open_ports:
      if ports in dangerous_ports:
         print(f"{ports} : Warning\n")
         psutil.Process(conn.pid).terminate()
         print("They are close now")
      else:
         print(f"{ports} : Ok\n")
else:
   print("0 port open")










